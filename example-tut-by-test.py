#!/usr/bin/env python3
# -- coding: utf-8 --
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import os
from time import sleep
import unittest
import datetime

PATH = lambda p: os.path.abspath(os.path.join(os.path.dirname(__file__), p))

#LOGIN = input("Введите адрес электронной почты TUT.by: ")
#PASSWORD = input("Введите пароль от электронной почты " + LOGIN + ": ")

LOGIN = "e.kharetsky@tut.by"
PASSWORD = "Leming91"


class TutbyTest(unittest.TestCase):
    print("Начинаем тестирование. Это может занять несколько минут. Ожидайте...")
    dt = str(datetime.datetime.today())
    DATE = dt[0:10] + "_" + dt[11:13] + "-" + dt[14:16] + "-" + dt[17:19]
    logFile = open(PATH("tut-by-tests") + "log-" + DATE + ".txt", 'w')
    LOG_CROSS_LINE = "============================================================\n"
    CROSS_LINE = "============================================================"

    yes_answers = ["ДА", "да", "Да", "дА", "lf", "LF", "Lf", "lF", "Yes", "yes", "yEs", "yeS", "YES", "YEs", "yES",
                   "YeS"]
    no_answers = ["Нет", "нЕт", "неТ", "нет", "НЕТ", "НеТ", "НЕт", "нЕТ", "Ytn", "yTn", "ytN", "ytn", "YTN", "YtN",
                  "YTn", "yTN", "No", "nO", "NO", "no"]

    def setUp(self):
        self.driver = webdriver.Chrome(PATH("chromedriver.exe"))
        self.driver.get("http://www.tut.by")
        self.driver.implicitly_wait(10)

    def tearDown(self):
        self.driver.quit()

    def startTest(self, test_name):
        """
        Функция записи лога старта теста

        :param test_name: название теста
        """
        self.logFile.write(self.LOG_CROSS_LINE)
        self.logFile.write("ТЕСТ " + test_name + " НАЧАЛСЯ\n")
        self.logFile.write('\n')
        print(self.CROSS_LINE)
        print("ТЕСТ " + test_name + " НАЧАЛСЯ")
        print()

    def doneTest(self, test_name):
        """
        Функция записи лога успешного окончания теста

        :param test_name: название теста
        """
        self.logFile.write("\n")
        self.logFile.write("ТЕСТ " + test_name + " УСПЕШНО ЗАВЕРШЕН\n")
        self.logFile.write(self.LOG_CROSS_LINE)
        print()
        print("ТЕСТ " + test_name + " УСПЕШНО ЗАВЕРШЕН")
        print(self.CROSS_LINE)

    def authorization(self):
        """
        Функция авторизации пользователя

        Шаги:
            1. Нажать кнопку "Войти"
            2. Ввести данные в поле "Логин"
            3. Ввести данные в поле "Пароль"
            4. Нажать кнопку "Войти"

        """
        sleep(5)
        auth_btn = self.driver.find_element_by_class_name("enter")
        auth_btn.click()
        self.logFile.write("Кнопка 'Войти' нажата\n")
        print("Кнопка 'Войти' нажата")
        sleep(5)
        login_field = self.driver.find_element_by_name("login")
        login_field.send_keys(LOGIN)
        self.logFile.write("Вводим логин - " + LOGIN + "\n")
        print("Вводим логин - " + LOGIN)
        sleep(5)
        password_field = self.driver.find_element_by_name("password")
        password_field.send_keys(PASSWORD)
        self.logFile.write("Вводим пароль - " + PASSWORD + "\n")
        print("Вводим пароль - " + PASSWORD)
        sleep(5)
        enter_btn = self.driver.find_element_by_xpath("//*[@id='authorize']/div/div/div/form/div[4]/input")
        enter_btn.click()
        self.logFile.write("Кнопка 'Войти' нажата\n")
        print("Кнопка 'Войти' нажата")
        sleep(5)
        try:
            try:
                self.driver.find_element_by_class_name("uname")  # проверка успешного логина
                return
            except NoSuchElementException:
                pass
            auth_btn = self.driver.find_element_by_class_name(
                "enter")  # при неуспешном логине страница перезагрузится и нужно снова нажать на кнопку "Войти"
            auth_btn.click()
            sleep(3)
            self.driver.find_element_by_class_name("b-auth__error")  # проверка на наличие надписи с ошибкой
            print("Вы ввели неверное имя или пароль. Хотите повторить попытку? .... Да - Нет")
            __user_answer = input()  # просим ввести ответ на вопрос
            user_answer = str(__user_answer)
            for i in range(len(self.yes_answers)):
                if (user_answer.__eq__(self.yes_answers[i])):
                    print("Вы захотели начать тест заново. Пожалуйста введите данные.")
                    self.logFile.write("Вы захотели начать тест заново. Начинаем\n")
                    global LOGIN
                    global PASSWORD
                    mail = input("Введите новый электронный адрес TUT.by: ")
                    LOGIN = mail
                    password = input("Введите пароль от электронного адреса " + mail + ": ")
                    PASSWORD = password
                    self.driver.refresh()  # перезагружаем страницу
                    self.authorization()  # повторяем авторизацию
                    break
            for i in range(len(self.no_answers)):
                if (user_answer.__eq__(self.no_answers[i])):
                    self.driver.close()
                    self.driver.quit()
                    self.fail()

        except NoSuchElementException:
            print("Нужно фиксить баг")
        sleep(5)

    def toMail(self):
        """
        Функция шагов до почтового ящика

        Шаги:
            1. Открыть меню пользователя.
            2. Нажать на кнопку "Почта"

        """
        user_btn = self.driver.find_element_by_xpath("//*[@id='authorize']/div/a")
        user_btn.click()
        self.logFile.write("Открыто меню пользователя " + user_btn.text + "\n")
        print("Открыто меню пользователя " + user_btn.text)
        sleep(5)
        mail_btn = self.driver.find_element_by_link_text("Почта")
        mail_btn.click()
        self.logFile.write("Кнопка 'Почта' нажата\n")
        print("Кнопка 'Почта' нажата")
        sleep(5)

    def test_00_auth(self):
        """
        ТЕСТ АВТОРИЗАЦИИ

        Шаги:
            1. Нажать кнопку "Войти"
            2. Ввести данные в поле "Логин"
            3. Ввести данные в поле "Пароль"
            4. Нажать кнопку "Войти"

        """
        self.startTest("АВТОРИЗАЦИЯ")
        self.authorization()
        try:
            self.logFile.write("Проверяем страницу\n")
            print("Проверяем страницу")
            self.driver.find_element_by_class_name("uname")
        except NoSuchElementException:
            self.logFile.write("Мы не попали на нужную страницу\n")
            print("Мы не попали на нужную страницу")
            self.driver.close()
            self.driver.quit()
            self.fail()
        self.doneTest("АВТОРИЗАЦИЯ")

    def test_01_mail_check(self):
        """
        ТЕСТ ПРОВЕРКИ ВХОДЯЩИХ ПИСЕМ

        Шаги:
            1. Нажать кнопку "Войти"
            2. Ввести данные в поле "Логин"
            3. Ввести данные в поле "Пароль"
            4. Нажать кнопку "Войти"
            5. Открыть меню пользователя.
            6. Нажать на кнопку "Почта"

        """
        self.startTest("ПРОВЕРКА ВХОДЯЩИХ ПИСЕМ")
        self.authorization()
        self.toMail()
        try:
            self.logFile.write("Проверка на наличие писем\n")
            print("Проверка на наличие писем")
            self.driver.find_element_by_xpath(
                "//*[@id='nb-1']/body/div[2]/div[4]/div/div[2]/div[3]/div[4]/div[1]/div/div/div[3]/div/div/div/div[1]")  # проверяем на наличие элемента говорящего о отсутствии писем
            self.logFile.write("В почтовом ящике нет писем\n")
            print("В почтовом ящике нет писем")
        except NoSuchElementException:
            self.logFile.write("Входящие найдены. Ведем подсчет\n")
            print("Входящие найдены. Ведем подсчет")
            is_mail = self.driver.find_element_by_xpath(
                "//*[@id='nb-1']/body/div[2]/div[4]/div/div[2]/div[2]/div[2]/div[2]/div[1]/a[1]")
            mail_text = is_mail.get_attribute("title")
            self.logFile.write("У пользователя есть " + mail_text + "\n")
            print("У пользователя есть " + mail_text)
        self.doneTest("ПРОВЕРКА ВХОДЯЩИХ ПИСЕМ")


if __name__ == "__main__":
    unittest.main()
